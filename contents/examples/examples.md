# Examples
## Compute $\int_0^\pi \cos^2 \theta d\theta = \frac {\pi}{2} \approx \sum_{i=1}^{10} \cos^2 \theta_i \Delta \theta$
-[ipynb-1](./compute_int_cos2theta.ipynb)  
-[ipynb-2](https://github.com/githubdcw/electromagnetics-X/blob/main/ipynb/compute_int_cos2theta.ipynb)  
## Compute magnetic flux from magnetic vector potential  
-[ipynb-1](./magnetic_vector_potential.ipynb)  
-[ipynb-2](https://github.com/githubdcw/electromagnetics-X/blob/main/ipynb/magnetic_vector_potential.ipynb)
## Compute inductance of a circular loop  
-[ipynb-1](./inductance_of_a_circular_loop.ipynb)  
-[ipynb-2](https://github.com/githubdcw/electromagnetics-X/blob/main/ipynb/inductance_of_a_circular_loop.ipynb)
