[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 2](ch2TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |  

|[ก่อนหน้า](ch2TH.md)| [ต่อไป](ch2-02TH.md) |
| ---------- | ---------- |    

## 2.1 กฎของคูลอมบ์ (Coulomb's Law)   
ในหัวข้อนี้ เราจะเรียนเกี่นวกับแรงระหว่างประจุไฟฟ้าและเรารู้ว่าประจุไฟฟ้าเกิดจากอนุภาคสองชนิดคืออิเล็กตรอน (electron) ซึ่งมีประจุลบ และโปรตอน (proton) ซึ่งมีประจุบวก ทั้งสองอนุภาคมีขนาดและมวลซึ่งตาม[กฎความโน้มถ่วงสากลของนิวตัน](https://th.wikipedia.org/wiki/กฎความโน้มถ่วงสากลของนิวตัน) ([Newton's law of universal gravitation](https://en.wikipedia.org/wiki/Newton%27s_law_of_universal_gravitation))   จะมีแรงโน้มระหว่างอนุภาคด้วย ใน Python notebook ด้านล่างแสดงการเปรียบเทียบแรงทางไฟฟ้ากับแรงโน้มถ่วง (Comparison of electrical force and gravitational force between 2 particles, i.e. electron or proton).  
- [Python notebook](./asset/coulomb-law-examples.ipynb)  
- Python notebook ใน Kaggle: [https://www.kaggle.com/denchai/coulomb-law-examples](https://electromagneticsxchannel.page.link/DQbR)  
  
|[ก่อนหน้า](ch2TH.md)| [ต่อไป](ch2-02TH.md) |
| ---------- | ---------- |    


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  



