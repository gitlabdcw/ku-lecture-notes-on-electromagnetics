[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | -------- |    

|[ก่อนหน้า](../contentTH.md)| [ต่อไป](ch2-01TH.md) |
| ---------- | ---------- |    

# บทที่ 2: การคำนวณหาสนามไฟฟ้าด้วยกฎของคูลอมบ์   
ในบทที่ 2 เราเรียนเรื่อง...
## [2.1 กฎของคูลอมบ์ (Coulomb's Law)](ch2-01TH.md)  
## [ตัวอย่าง บทที่ 2](ch2-examplesTH.md)  


|[ก่อนหน้า](../contentTH.md)| [ต่อไป](ch2-01TH.md) |
| ---------- | ---------- |    


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


