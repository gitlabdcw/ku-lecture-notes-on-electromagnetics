# Examples
## Compute Electric field produced by a circular line charge of $\rho_l$ C/m centered at the origin with the raduis of 'a'.
-[ipynb-1 (on Gitlab)](./asset/computer_e_x_axis_circular.ipynb)  
-[ipynb-2 (on Github)](https://github.com/githubdcw/electromagnetics-X/blob/main/ipynb/computer_e_x_axis_circular.ipynb)  
