|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- |    

1.3.5 [ตัวอย่างการหาความยาวของส่วนของพาราโบลา](ch1-03-05TH.md)  
<a href="https://github.com/githubdcw/github-KU-Lecture-notes-on-Electromagnetics/blob/main/example_find_length_and_weight_curve.ipynb" target="_blank">Click</a> เพื่อเปิดในหน้าต่างใหม่ **Click** [<img src="https://camo.githubusercontent.com/84f0493939e0c4de4e6dbe113251b4bfb5353e57134ffd9fcab6b8714514d4d1/68747470733a2f2f636f6c61622e72657365617263682e676f6f676c652e636f6d2f6173736574732f636f6c61622d62616467652e737667">](https://github.com/githubdcw/github-KU-Lecture-notes-on-Electromagnetics/blob/main/example_find_length_and_weight_curve.ipynb) เพื่อให้แสดงผลใน Google Colab ได้ถูกต้อง  





|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- | 
