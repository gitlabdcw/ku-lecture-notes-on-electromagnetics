[$\footnotesize \copyright 2020 \: \text {Denchai Worasawate}$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |    

|[ก่อนหน้า](ch1-07TH.md)| [ต่อไป](ch1-08-01TH.md) |
| ---------- | ---------- |    

## 1.8 การแปลงเวกเตอร์ระหว่างพิกัดต่างๆ (Coordinate Transformation of Vector Components)  
เวกเตอร์เป็นปริมาณที่กระทำที่จุดๆหนึ่งเช่นมีแรง $\mathbf F_{1a}$ และ $\mathbf F_{1b}$ กระทำที่จุด $P_1(x_1,y_1,z_1)$ และ แรง $\mathbf F_2$ กระทำที่จุด $P_2(x_2,y_2,z_2)$  
เช่น $\mathbf F_{1a} = 3 \mathbf a_x + 4 \mathbf a_y + 5 \mathbf a_z$ และ $\mathbf F_{1b} = 3 \mathbf a_x + 4 \mathbf a_y - 5 \mathbf a_z$ เราสามารถหาแรงรวม $\mathbf F_{1}$ ที่จุด $P_1$ ได้โดย
```math
\mathbf F_{1} = \mathbf F_{1a}+ \mathbf F_{1b} \newline  

\mathbf F_{1} = \left(3 \mathbf a_x + 4 \mathbf a_y + 5 \mathbf a_z \right)+\left(3 \mathbf a_x + 4 \mathbf a_y - 5 \mathbf a_z \right) \newline  

\mathbf F_{1} = \left(6 \mathbf a_x + 8 \mathbf a_y\right) \newline
```  
เราเขียนเวกเตอร์  $\mathbf F_{1}$ นี้โดยการใช้เวกเตอร์หนึ่งหน่วยของระบบพิกัดฉาก ถ้า $\mathbf F_{2} = 3 \mathbf a_x + 4 \mathbf a_y - 5 \mathbf a_z $ เราสามารถหาผลต่างของแรงที่จุด $P_1$ กับจุด $P_2$ หรือ $\Delta \mathbf F$ ได้โดย  

```math
\Delta \mathbf F = \mathbf F_{1} - \mathbf F_{2} \newline  
\Delta \mathbf F = \left(6 \mathbf a_x + 8 \mathbf a_y\right) -\left(3 \mathbf a_x + 4 \mathbf a_y - 5 \mathbf a_z\right) \newline
\Delta \mathbf F = 3 \mathbf a_x + 4 \mathbf a_y + 5 \mathbf a_z \newline
``` 
แต่เราสามารถเขียนเวกเตอร์โดยการแยกเป็นสามทิศทางใดๆที่ตั้งฉากกันซึ่งเราอาจเขียนอยู่ในรูปของเวกเตอร์หนึ่งหน่วยของพิกัดแบบทรงกระบอกหรือแบบทรงกลมได้ ซึ่งถ้าเราให้ $\mathbf F_{1} = 10 \mathbf a_\rho $ และ  $\mathbf F_{2} = 5 \mathbf a_\rho -5 \mathbf a_z$ ถ้าเราต้องการหาผลต่างของแรงที่จุด $P_1$ กับจุด $P_2$ หรือ $\Delta \mathbf F$ เราไม่สามารถนำเวกเตอร์ในที่เขียนในรูปแบบนี้มาลบกันได้โดยตรง
```math
\Delta \mathbf F = \mathbf F_{1} - \mathbf F_{2} \newline  
\Delta \mathbf F \neq \left(10 \mathbf a_\rho \right) -\left(5 \mathbf a_\rho -5 \mathbf a_z \right) \newline
\Delta \mathbf F \neq 5 \mathbf a_\rho +5 \mathbf a_z  
```  
เนื่องจาก $\mathbf a_\rho $ และ $\mathbf a_\phi $ ที่ต่างจุดกันจะมีทิศทางต่างจาก ดังรูป $(a)$ เราเห็นว่า $\mathbf F_{1}$ มีส่วนที่ชี้ในทิศ $a_\rho $ และ $ \mathbf F_{2}$ มีส่วนที่ชี้ในทิศ $a_\rho $ เหมือนกัน แต่เรานำส่วนที่ชี้ในทิศ $a_\rho $ มาลบกันไม่ได้เพราะ $a_\rho $ ที่จุดที่ $P_1$ อาจไม่เท่ากับ $a_\rho $ ที่จุดที่ $P_2$  เราจะต้องทราบตำแหน่งของจุด $P_1$ และจุด $P_2$ ก่อนถึงจะทราบทิศทางของเวกเตอร์ และต้องแปลงการเขียนเวกเตอร์ให้อยู่รูปแบบของพิกัดฉาก หรือให้อยู่ในรูปของ $\mathbf a_x$, $\mathbf a_y$ และ $\mathbf a_z$ ก่อนเพราะ $\mathbf a_x$, $\mathbf a_y$ และ $\mathbf a_z$ ที่ตำแหน่งต่างกันมีทิศทางเหมือนกัน ดังนั้นเราสามารถนำเวกเตอร์มาบวกหรือลบกันได้     
<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/cyline_cartesian.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(a) แสดงทิศทางของเวกเตอร์หนึ่งหน่วยที่จุด $P$ และจุด $Q$ (ดัดแปลงจาก [Hayt 2011](#Hayt))


การแปลงชื่อของเวกเตอร์จากพิกัดหนึ่ง เราจะต้องเขียนเวกเตอร์หนึ่งหน่วยของพิกัดหนึ่งในรูปของเวกเตอร์หนึ่งหน่วยของอีกพิกัดหนึ่งเช่นถ้าเราต้องการเขียน $\mathbf F_{1}\text{ ให้อยู่ในรูปของพิกัดแบบทรงกระบอกเราจะต้องรู้ว่าจะเขียน }\mathbf a_x\text{, }\mathbf a_y\text{, และ }\mathbf a_z\text{ ให้อยู่ในรูปของ }\mathbf a_\rho\text{, }\mathbf a_\phi\text{, และ }\mathbf a_z$ ได้อย่างไร  
### การหาส่วนของเวกเตอร์ในทิศทางใดทิศทางหนึ่ง  
ถ้าเรานำเวกเตอร์มาคูณแบบดอทกับเวกเตอร์หนึ่งหน่วยในทิศทางหนึ่งเราจะได้ส่วนของเวกเตอร์ที่ชี้ไปในทิศทางนั้นๆ เช่นถ้าเราต้องการทราบส่วนของ $\mathbf F_{1}\text{ ที่ชี้ไปในทิศทาง }x\text{ หรือเราจะเรียกว่า }F_x= \mathbf F_{1} \cdot \mathbf a_x$
```math
F_x= \mathbf F_{1} \cdot \mathbf a_x \newline  
F_x= \left(6 \mathbf a_x + 8 \mathbf a_y\right)  \cdot \mathbf a_x \newline  
F_x= \left( 6 \mathbf a_x \cdot \mathbf a_x + 8 \mathbf a_y \cdot \mathbf a_x \right) \newline
F_x= \left(6\cdot 1 + 8 \cdot 0 \right) \newline
```
### การเขียนเวกเตอร์หนึ่งหน่วยในพิกัดหนึ่งในรูปของเวกเตอร์หนึ่งหน่วยในพิกัดฉาก
1.8.1 [การเขียนเวกเตอร์หนึ่งหน่วยในพิกัดแบบทรงกระบอกในรูปของเวกเตอร์หนึ่งหน่วยในพิกัดฉาก](ch1-08-01TH.md)  
1.8.2 [การเขียนเวกเตอร์หนึ่งหน่วยในพิกัดแบบทรงกลมในรูปของเวกเตอร์หนึ่งหน่วยในพิกัดฉาก](ch1-08-02TH.md)  

### ตัวอย่างการแปลงเวกเตอร์ระหว่างพิกัดต่างๆ  
1.8.3 [ตัวอย่างการแปลงเวกเตอร์ระหว่างพิกัดแบบทรงกระบอกกับพิกัดฉาก](ch1-08-03TH.md)  
1.8.4 [ตัวอย่างการแปลงเวกเตอร์ระหว่างพิกัดแบบทรงกลมกับพิกัดฉาก](ch1-08-04TH.md)  
1.8.5 [ตัวอย่างการแปลงเวกเตอร์ระหว่างพิกัดแบบทรงกระบอกกับพิกัดแบบทรงกลม](ch1-08-05TH.md)  
1.8.6 [ตัวอย่างการหางานจากการออกแรงดันก้อนหินต้านแรงเสียดทาน โดยใช้พิกัดทรงกระบอก](ch1-08-06TH.md)  

  


อ้างอิง  
- <a name="Hayt"></a> Hayt, W. H., & Buck, J. A. (2011). Engineering Electromagnetics (8th ed.). McGraw-Hill Professional.

 

|[ก่อนหน้า](ch1-07TH.md)| [ต่อไป](ch1-08-01TH.md) |
| ---------- | ---------- |    
   


[$\footnotesize \copyright 2020 \: \text {Denchai Worasawate}$](https://www.researchgate.net/profile/Denchai-Worasawate)  


