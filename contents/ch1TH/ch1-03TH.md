
[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |    

|[ก่อนหน้า](ch1-02TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- |    

## 1.3 ระบบพิกัดแบบคาร์ทีเซียน (Cartesian Coordinate System)  
ระบบพิกัดเป็นการระบุจุดต่างๆเพื่อให้ง่ายต่อการอ้างถึง ระบบพิกัดมีอยู่หลายระบบ แต่เราจะพูดถึงเพียง 3 ระบบในวิชาคือ ระบบพิกัดแบบคาร์ทีเซียน ([Cartesian Coordinate System](https://en.wikipedia.org/wiki/Cartesian_coordinate_system)) ระบบพิกัดแบบทรงกระบอก ([Cylindrical Coordinate System](https://en.wikipedia.org/wiki/Cylindrical_coordinate_system)) และ [ระบบพิกัดแบบทรงกลม](https://th.wikipedia.org/wiki/ระบบพิกัดทรงกลม) ([Spherical Coordinate System](https://en.wikipedia.org/wiki/Spherical_coordinate_system))  
ระบบพิกัดที่ง่ายที่สุดคือระบบพิกัดแบบคาร์ทีเซียน หรือ บางทีเราจะเรียกว่าระบบพิกัดฉาก (Rectangular Coordinate System) ซึ่งประกอบด้วย แกน 3 แกนที่ตั้งฉากซึ่งกันและกัน ได้แก่ แกน $`x \; y \; \text{และ} \; z`$
ซึ่งมีลักษณะดังรูป $`(a)`$ แกน 3 แกนที่ตั้งฉากกันตัดกันที่จุด $`x=0 \; y=0 \; \text{และ} \; z=0`$ จุดนี้เรียกว่าจุดกำเนิด (origin) หรือเรียกชื่อว่าจุด $`(0,0,0)`$ ทำให้เกิดระนาบหลัก 3 ระนาบที่ตั้งฉากกันคือ $`\text{ระนาบ} \; xy \; \text{หรือ ระนาบ} \; z=0`$ $`\text{ระนาบ} \; yx \; \text{หรือ ระนาบ} \; x=0`$ $`\text{ระนาบ} \; xz \; \text{หรือ ระนาบ} \; y=0`$   
แกน $`x \; y \; \text{และ} \; z`$ ต้องเป็นไปตามกฎมือขวา ([right hand rule](https://en.wikipedia.org/wiki/Right-hand_rule)) ตามรูปด้านล่าง เมื่อ เวกเตอร์ $`\mathbf a`$ เป็น ทิศทาง $`x`$ เวกเตอร์ $`\mathbf b`$ เป็น ทิศทาง $`y`$ และ เวกเตอร์ $`\mathbf c = \mathbf a \times \mathbf b`$ เป็น ทิศทาง $`z`$ เครื่องหมาย $`\times`$ คือการคูณเวกเตอร์ที่เรียกว่า[การคูณแบบครอส](./ch1-05-2TH.md) (cross product) ซึ่งเราจะได้ว่า   $`\mathbf a_z = \mathbf a_x \times \mathbf a_y \; \text{เมื่อ} \; \mathbf a_x`$ เป็นเวกเตอร์หนึ่งหน่วย (unit vector) ที่มีทิศทางไปตามแกน $`x`$ $`\mathbf a_y`$ เป็นเวกเตอร์หนึ่งหน่วยที่มีทิศทางไปตามแกน $`y`$ และ $`\mathbf a_z`$ เป็นเวกเตอร์หนึ่งหน่วยที่มีทิศทางไปตามแกน $`z`$   
<img src="https://upload.wikimedia.org/wikipedia/commons/d/d2/Right_hand_rule_cross_product.svg" width="100" >  
แต่ละจุดในบริเวณ 3 มิติจะเรียกชื่อตามระยะทางจากจุดกำเนิดคือ $`(x,y,z)`$ ดังรูปด้านล่าง  

<img src="https://upload.wikimedia.org/wikipedia/commons/6/69/Coord_system_CA_0.svg" align="center" width=200 alt="" />  

ในรูป $`(b)`$ $` \text{จุด} \; P \; \text{มีชื่อเรียกว่า} \; P(1,2,3)`$ เนื่องจากจุดห่างจากจุดกำเนิดในทิศทางบวก $`x`$ อยู่ 1 หน่วยในทิศทางบวก $`y`$ อยู่ 2 หน่วยและ ในทิศทางบวก $`z`$ อยู่ 3 หน่วย และ $` \text{จุด} \; Q \; \text{มีชื่อเรียกว่า} Q(2,-1,1)`$ เนื่องจากจุดห่างจากจุดกำเนิดในทิศทางบวก $`x`$ อยู่ 2 หน่วยในทิศทางลบ $`y`$ อยู่ 1 หน่วยและ ในทิศทางบวก $`z`$ อยู่ 1 หน่วย  
ในรูป $`(c)`$ จุด $`P'`$ เป็นจุดที่เคลื่อนจากจุด $`P(x,y,z)`$ ออกมาเป็นระยะทางสั้นๆ เรียกว่า ความยาวดิฟเฟอเรนเชียล (differential length) ในทิศทาง  $`x \; y`$ และ $`z`$ เรียกว่า  $`dx \; dy`$ และ $`dz`$ ดังนั้นจุด จุด $`P'`$ คือจุด $`P'(x+dx,y+dy,z+dz)`$ ความยาวดิฟเฟอเรนเชียลจากจุด $`P`$ ไปจุด $`P'`$ คือ $`dl = \sqrt{dx^2+dy^2+dz^2}`$  
ในรูป $`(c)`$ มีระนาบอยู่ 6 ระนาบคือ 3 ระนาบที่ตัดกันที่จุด $`P(x,y,z)`$  และ 3 ระนาบที่ตัดกันที่จุด $`P'(x+dx,y+dy,z+dz)`$ รวมกันเป็นกล่องสี่เหลี่ยมเล็กๆที่มีปริมาตรเท่ากับ  $`dv = dx dy dz`$ พื้นผิวของกล่องสี่เหลี่ยมเล็กๆ เรียกว่า พื้นผิวดิฟเฟอเรนเชียล (differential area) ซึ่งมีอยู่สามพื้นผิวคือ $`ds_x = dy dz \; ds_y = dx dz`$ และ $`ds_z = dx dy`$  

### ตัวอย่างการประยุกต์ใช้พิกัดฉาก  
1.3.0 [การใช้ www.integral-calculator.com](ch1-03-00TH.md)  
1.3.1 [ตัวอย่างการหาความยาวของครึ่งวงกลมเส้นโค้ง](ch1-03-01TH.md)  
1.3.2 [ตัวอย่างการหาพื้นที่ครึ่งวงกลม](ch1-03-02TH.md)  
1.3.3 [ตัวอย่างการหาปริมาตรครึ่งทรงกลม](ch1-03-03TH.md)  
1.3.4 [ตัวอย่างการน้ำหนักจากความหนาแน่น](ch1-03-04TH.md)  
1.3.5 [ตัวอย่างการหาความยาวของส่วนของพาราโบลา](ch1-03-05TH.md)  
  
|[ก่อนหน้า](ch1-02TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- |    

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/cartesian_a.svg" align="center" width=50% alt="" />
</td></tr>
<tr><td align="center" width="9999">
(a)
</td></tr>
<tr><td align="center" width="9999">
<img src="./asset/cartesian_b.svg" align="center" width=50% alt="" />
</td></tr>
<tr><td align="center" width="9999">
(b)
</td></tr>
<tr><td align="center" width="9999">
<img src="./asset/cartesian_c.svg" align="center" width=50% alt="" />
</td></tr>
<tr><td align="center" width="9999">
(c)
</td></tr>
</table>  

|[ก่อนหน้า](ch1-02TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- |    


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


