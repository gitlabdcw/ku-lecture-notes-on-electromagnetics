|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- |    

### 1.3.4 [ตัวอย่างการน้ำหนักจากความหนาแน่น](ch1-03-04TH.md)  
- ตัวอย่างการหาน้ำหนักของเส้นลวด
	- ความหนาแน่น= $6x$ kg/m เป็น linear function
	<a href="./ipynb/example_straight_line_linear_density_integration.ipynb" target="_blank">Click</a> เพื่อเปิดในหน้าต่างใหม่ **Click**   
	หมายเหตุ Gitlab อาจแสดงผล สมการ ใน ipynb ได้ไม่ถูกต้อง   
	[<img src="./ipynb/openincolab.svg">](https://github.com/githubdcw/github-KU-Lecture-notes-on-Electromagnetics/blob/main/example_straight_line_linear_density_integration.ipynb) เพื่อให้แสดงผลใน Google Colab ได้ถูกต้อง  
	- ความหนาแน่น= $x^2$ kg/m เป็น nonlinear function
	<a href="./ipynb/example_straight_line_density_integration.ipynb" target="_blank">Click</a> เพื่อเปิดในหน้าต่างใหม่ **Click**   
	หมายเหตุ Gitlab อาจแสดงผล สมการ ใน ipynb ได้ไม่ถูกต้อง   
	[<img src="./ipynb/openincolab.svg">](https://github.com/githubdcw/github-KU-Lecture-notes-on-Electromagnetics/blob/main/example_straight_line_density_integration.ipynb) เพื่อให้แสดงผลใน Google Colab ได้ถูกต้อง  



|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- | 
