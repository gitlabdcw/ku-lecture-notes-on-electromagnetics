[$\footnotesize \copyright 2020 \: \text {Denchai Worasawate}$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |    

|[ก่อนหน้า](ch1-06TH.md)| [ต่อไป](ch1-07-00TH.md) |
| ---------- | ---------- |    

## 1.7 พิกัดทรงกลม (Spherical Coordinates)  
ระบบพิกัดเป็นการระบุจุดต่างๆเพื่อให้ง่ายต่อการอ้างถึง หรือเป็นการเรียกชื่อจุด ระบบพิกัดมีอยู่หลายระบบ เราจะพูดถึงพิกัดฉากไปแล้วในหัวข้อ [1.3 ระบบพิกัดแบบคาร์ทีเซียน (Cartesian Coordinate System)](Ch1-03TH.md)  และพิกัดทรงกระบอกในหัวข้อ [1.6 พิกัดทรงกระบอก (Circular Cylindrical Coordinates)](Ch1-06TH.md)   
ในพิกัดฉาก เราเรียกชื่อจุดด้วยระยะจากจุดกำเนิดโดยเราวัดระยะ 3 แนวคือในแนวแกน $x \text{ แกน }y\text{ และแกน }z\text{ เช่นในรูป }(a)\text{ ด้านล่าง ถ้าตำแหน่งของจุด }P \text{ มีระยะ }x_1\text{ }y_1\text{ และ }z_1$ ห่างจากจุดกำเนิดในแกน $x\text{ }y\text{ และ }z\text{ ตามลำดับ จุด }P$ จะเป็นจุดตัดของ 3 ระนาบคือ 
1. ระนาบ $x=x_1$   
2. ระนาบ $y=y_1$    
3. ระนาบ $z=z_1$   

เราจะเรียกชื่อจุด $P\text{ ว่า }(x_1,y_1,z_1)$  

### ตัวแปรและเวกเตอร์หนึ่งหน่วยใรพิกัดแบบทรงกระบอก  
ในหัวข้อนี้เราจะพูดถึง[ระบบพิกัดแบบทรงกลม](https://th.wikipedia.org/wiki/ระบบพิกัดทรงกลม) ([Spherical Coordinate System](https://en.wikipedia.org/wiki/Spherical_coordinate_system)) 
จากรูป (a) ตำแหน่งของจุด $P$ เป็นจุดตัดของ 3 พื้นผิวคือ 
1. พื้นผิว (ทรงกลมม, sphere) $r= r_1\text{ ซึ่งเป็นพื้นผิวที่มีระยะห่างจากจุดกำเนิด เท่ากับ  }r_1$  
2. พื้นผิว (กรวย, cone) $\theta = \theta_1\text{ ซึ่งเป็นพื้นผิวที่ทำมุม } \theta_1\text{ กับแกน }z$   
3. พื้นผิว (ระนาบ, plane) $\phi=\phi_1\text{ ซึ่งเป็นพื้นผิวที่ทำมุม }\phi_1\text{ กับแกน }x$   

เราจะเรียกชื่อจุด $P\text{ ว่า }(r_1,\theta_1, \phi_1)$ ได้อีกชื่อหนึ่ง   

ถ้าเราเห็นชื่อของจุดเป็นเลขสามตัวเราต้องถามว่าเป็นพิกัดอะไรเพราะเลขแต่ละตัวของแต่ละพิกัดมีความหมายต่างกัน

พิกัดแบบทรงกลมมีตัวแปรบอกตำแหน่งสามตัวคือ $r\text{ }\theta\text{ และ }\phi$ ซึ่งประกอบด้วยแกน 3 แกนที่ตั้งฉากซึ่งกันและกันคล้ายๆกับในพิกัดฉาก ได้แก่แกน $r\text{ }\theta\text{ และ }\phi\text{ โดยที่แกน }r\text{ และแกน }\theta\text{ เป็นแกนใหม่แสดงในรูป }(b)\text{ ส่วนแกน }\phi$ จะเหมือนกับแกนในพิกัดแบบทรงกระบอก ทิศทางของแกนจะขึ้นอยู่กับตำแหน่งของจุด ดังนั้นเราจะมีตัวแปรสำหรับเรียกชื่อจุดเพิ่มขึ้น 2 ตัวคือ $r\text{ กับ }\theta$  

ในรูป $(b)$ ทิศทางของแกนจะบอกด้วยเวกเตอร์หนึ่งหน่วย ซึ่งกำกับอยู่ที่จุดโดยที่ $\mathbf a_r\text{ }\mathbf a_\theta\text{ และ }\mathbf a_\phi\text{ บอกทิศทางบวกของแกน }r\text{ }\theta\text{ และ }\phi$ ตามลำดับ เวกเตอร์หนึ่งหน่วยนี้ต้องเป็นไปตามกฎมือขวา ([right hand rule](https://en.wikipedia.org/wiki/Right-hand_rule)) ตามรูปด้านล่าง เมื่อ เวกเตอร์ $\mathbf a\text{ เป็นทิศทางของ }\mathbf a_r\text{ เวกเตอร์ }\mathbf b\text{ เป็นทิศทางของ }\mathbf a_\theta\text{ และ เวกเตอร์ }\mathbf c = \mathbf a \times \mathbf b\text{ เป็น ทิศทางของ }\mathbf a_\phi\text{ เครื่องหมาย }\times$ คือการคูณเวกเตอร์ที่เรียกว่า[การคูณแบบครอส](./ch1-05-2TH.md) (cross product) ซึ่งเราจะได้ว่า   $\mathbf a_\phi = \mathbf a_r \times \mathbf a_\theta$      
<img src="https://upload.wikimedia.org/wikipedia/commons/d/d2/Right_hand_rule_cross_product.svg" width="100" >  

ทิศทางของแกน $r$ แกน $\theta$ และแกน $\phi$ จะเปลี่ยนทิศไปตามตำแหน่งของจุดเช่น แกนที่จุด $P$ และ แกนที่จุด $Q$ ในรูป $(d)$ มีทิศทางต่างกัน  

ในรูป $(c)\text{ จุด }P'\text{ เป็นจุดที่เคลื่อนจากจุด }P(r,\theta,\phi)$ ออกมาเป็นระยะทางสั้นๆ เรียกว่าความยาวดิฟเฟอเรนเชียล 
(differential length) ในทิศทาง $r \; \theta \text{ และ }\phi\text{ เรียกว่า }dr\;r d\theta\text{ และ }\rho d\phi\text{ หรือ }r \sin \theta d\phi$
เราต้องระวังว่า$d\theta\text{ และ }d\phi$ คือการเปลี่ยนแปลงมุมมีหน่วยเป็นเรเดียน ไม่ใช่ระยะทางที่มีหน่วยเป็นเมตร เราจะต้องคูณรัศมีของการเปลี่ยนแปลงมุมเพื่อหาการเปลี่ยนแปลงของระยะทางโค้งๆ 
เช่นดาวเทียม2ดวงที่หมุนไป $d\phi$ เท่าๆกัน ดาวเทียมดวงที่อยู่ห่างจากโลกมากกว่าจะเคลื่อนที่เป็นระยะทางมากกว่า 
รัศมีของมุม$\theta$ คือระยะจากจุดกำเนิดถึงจุด$P\text{ หรือระยะ }r$
แต่รัศมีของมุม$\phi$ คือระยะห่างจากแกน $z\text{หรือระยะ}\rho$ 
ซึ่งถ้าเราดูรูป$(d)$ จะเห็นว่า $\rho= r \sin \theta$  
$\text{ดังนั้นจุด }P'\text{ จะมีชื่อในพิกัดแบบทรงกระบอกว่า }P'(r+dr,\theta+d\theta. \phi+d\phi)\text{ โดยมีความยาวดิฟเฟอเรนเชียลจากจุด }P\text{ ไปจุด }P'\text{ คือ }dl = \sqrt{dr^2+\left( r d\theta \right)^2+\left( r \sin \theta d\phi \right)^2}$  

$\text{ในรูป} (c)\text{ มีระนาบอยู่ 6 ระนาบคือ 3 ระนาบที่ตัดกันที่จุด }P(r,\theta,\phi)\text{ และ 3 ระนาบที่ตัดกันที่จุด}P'(r+dr,\theta+d\theta,\phi+d\phi)\text{ รวมกันเป็นกล่องสี่เหลี่ยมเล็กๆที่มีปริมาตรเท่ากับ }dv = dr \times r d\theta \times  r \sin \theta d\phi \text{ หรือย้าย }r\text{ และ }\sin \theta \text{ มาด้านหน้า เพื่อให้ง่ายต่อการอินทิเกรตจะได้ว่า }dv = r^2 \sin \theta  dr d\theta  d\phi$  

พื้นผิวของกล่องสี่เหลี่ยมเล็กๆ เรียกกว่า พื้นผิวดิฟเฟอเรนเชียล (differential area) ซึ่งมีอยู่สามพื้นผิวคือ 
1. $ds_r = r d\theta \cdot r \sin \theta  d\phi = r^2 \sin \theta d\theta d\phi$ 
2. $ds_\theta = dr \cdot r \sin \theta d\phi = r \sin \theta dr d\phi$
3. $ds_\phi = dr \cdot r d\theta = r dr d\theta$ 

### [การแปลงชื่อของจุดระหว่างพิกัดฉาก $(x,y,z)$   กับพิกัดแบบทรงกลม $(r,\theta,\phi)$](ch1-07-00TH.md)  

### ตัวอย่างการประยุกต์ใช้พิกัดแบบทรงกระบอก  
1.7.1 [ตัวอย่างการหาความยาวของเส้นโค้ง](ch1-07-01TH.md)  
1.7.2 [ตัวอย่างการหาพื้นที่ครึ่งวงกลม](ch1-07-02TH.md)  
1.7.3 [ตัวอย่างการหาปริมาตรครึ่งทรงกลม](ch1-07-03TH.md)  

|[ก่อนหน้า](ch1-06TH.md)| [ต่อไป](ch1-07-00TH.md) |
| ---------- | ---------- |    

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/sphere_ab.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(a) แสดงพื้นผิวที่ตัวแปรในพิกัดแบบทรงกลมมีค่าคงที่ (b) แสดงทิศทางของแกน $r\text{ แกน }\theta\text{ และ แกน }\phi$ ที่จุด P (ดัดแปลงจาก [Hayt 2011](#Hayt))  

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/sphere_c.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(c) แสดงความยาวดิฟเฟอเรนเชียล (differential length) ปริมาตร ดิฟเฟอเรนเชียล (differential volume) พื้นผิวดิฟเฟอเรนเชียล (differential surface) (ดัดแปลงจาก [Hayt 2011](#Hayt))

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/sphere_cartesian.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(d) แสดงการแปลงชื่อของจุดระหว่างพิกัดฉากกับพิกัดแบบทรงกระบอก (differential surface) (ดัดแปลงจาก [Hayt 2011](#Hayt))

อ้างอิง  
- <a name="Hayt"></a> Hayt, W. H., & Buck, J. A. (2011). Engineering Electromagnetics (8th ed.). McGraw-Hill Professional.

|[ก่อนหน้า](ch1-06TH.md)| [ต่อไป](ch1-07-00TH.md) |
| ---------- | ---------- |    


[$\footnotesize \copyright 2020 \: \text {Denchai Worasawate}$](https://www.researchgate.net/profile/Denchai-Worasawate)  


