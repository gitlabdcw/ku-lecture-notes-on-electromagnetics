|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-03-01TH.md) |
| ---------- | ---------- |    

## การใช้ [integral-calculator.com](https://www.integral-calculator.com/)
integral-calculator.com เป็นเว็บไซต์ที่ให้บริการหาปริพันธ์ไม่จำกัดเขต (indefinite integral) และ ปริพันธ์จำกัดเขต (definite integral) ให้เราได้โดยให้เราใส่สมการที่ต้องการหาปริพันธ์ โดยสามารถดูวิธีการใส่ในแท็บ Example  
ซึ่งจะเป็นประโยชน์ในการคำนวนหาปริมาณต่างๆในวิชานี้ รูปด้านล่างแสดงการใส่สมการเพื่อหาค่าอินทิกรัล  
<img src="./asset/Integral-Calculator.svg" align="center" width=600 alt="" />  


|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-03-01TH.md) |
| ---------- | ---------- |    
