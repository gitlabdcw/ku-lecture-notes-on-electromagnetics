[$\footnotesize \copyright 2020 \: \text {Denchai Worasawate}$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |    

|[ก่อนหน้า](ch1-05TH.md)| [ต่อไป](ch1-06-00TH.md) |
| ---------- | ---------- |    

## 1.6 พิกัดทรงกระบอก (Circular Cylindrical Coordinates)  
ระบบพิกัดเป็นการระบุจุดต่างๆเพื่อให้ง่ายต่อการอ้างถึง หรือเป็นการเรียกชื่อจุด ระบบพิกัดมีอยู่หลายระบบ เราจะพูดถึงพิกัดฉากไปแล้วในหัวข้อ [1.3 ระบบพิกัดแบบคาร์ทีเซียน (Cartesian Coordinate System)](Ch1-03TH.md) เราเรียกชื่อจุดด้วยระยะจากจุดกำเนิดโดยเราวัดระยะ 3 แนวคือในแนว $\text{ แกน }x \text{ แกน } y \text{ และแกน } z \text{ เช่นในรูป } (a)$ ด้านล่าง ถ้าตำแหน่งของจุด $P \text{ มีระยะ } x_1 \enspace y_1 \text{ และ }z_1$ ห่างจากจุดกำเนิดในแกน $x \enspace y \text{ และ } z \text{ ตามลำดับ จุด }P$ จะเป็นจุดตัดของ 3 ระนาบคือ 
1. ระนาบ $x=x_1$   
2. ระนาบ $y=y_1$    
3. ระนาบ $z=z_1$   

เราจะเรียกชื่อจุด $P \text{ ว่า } (x_1,y_1,z_1)$  

### ตัวแปรและเวกเตอร์หนึ่งหน่วยในพิกัดแบบทรงกระบอก  
ในหัวข้อนี้เราจะพูดถึงระบบพิกัดแบบทรงกระบอก ([Cylindrical Coordinate System](https://en.wikipedia.org/wiki/Cylindrical_coordinate_system)) จากรูป (a) ตำแหน่งของจุด $P$ เป็นจุดตัดของ 3 พื้นผิวคือ 
1. พื้นผิว $\rho= \rho_1 \text{ ซึ่งเป็นพื้นผิวที่มีระยะห่างจากแกน }z \text{ เท่ากับ }\rho_1$  
2. พื้นผิว $\phi=\phi_1 \text{ ซึ่งเป็นพื้นผิวที่ทำมุมกับแกน }x \text{ อยู่ }\phi_1$  
3. พื้นผิว (ระนาบ) $z=z_1 \text{ ซึ่งเป็นพื้นผิวที่มีระยะ }z_1 \text{ ห่างจากจุดกำเนิด}$  

เราจะเรียกชื่อจุด $P$ ว่า $(\rho_1,\phi_1,z_1)$ ได้อีกชื่อหนึ่ง   

ถ้าเราเห็นชื่อของจุดเป็นเลขสามตัวเราต้องถามว่าเป็นพิกัดอะไรเพราะเลขแต่ละตัวของแต่ละพิกัดมีความหมายต่างกัน

พิกัดแบบทรงกระบอกมีตัวแปรบอกตำแหน่งสามตัวคือ $\rho \enspace \phi \text{ และ } z$ ซึ่งประกอบด้วยแกน 3 แกนที่ตั้งฉากซึ่งกันและกันคล้ายๆกับในพิกัดฉาก ได้แก่แกน $\rho \enspace \phi \text{ และ } z \text{ โดยที่ แกน } \rho \text{ และแกน }\phi \text{ เป็นแกนใหม่แสดงในรูป }(b)$ ทิศทางของแกนจะขึ้นอยู่กับตำแหน่งของจุด ดังนั้นเราจะมีตัวแปรสำหรับเรียกชื่อจุดเพิ่มขึ้น 2 ตัวคือ $\rho \text{ กับ }\phi$  

ในรูป $(b)$ ทิศทางของแกนจะบอกด้วยเวกเตอร์หนึ่งหน่วย ซึ่งกำกับอยู่ที่จุดโดยที่ $\mathbf a_\rho$ $\mathbf a_\phi$ และ $\mathbf a_z$ บอกทิศทางบวกของแกน $\rho$ $\phi$ และ $z$ ตามลำดับ เวกเตอร์หนึ่งหน่วยนี้ต้องเป็นไปตามกฎมือขวา ([right hand rule](https://en.wikipedia.org/wiki/Right-hand_rule)) ตามรูปด้านล่าง เมื่อ เวกเตอร์ $\mathbf a$ เป็นทิศทางของ $\mathbf a_\rho$ เวกเตอร์ $\mathbf b$ เป็นทิศทางของ $\mathbf a_\phi$ และ เวกเตอร์ $\mathbf c = \mathbf a \times \mathbf b$ เป็น ทิศทางของ $\mathbf a_z$ เครื่องหมาย $\times$ คือการคูณเวกเตอร์ที่เรียกว่า[การคูณแบบครอส](./ch1-05-2TH.md) (cross product) ซึ่งเราจะได้ว่า   $\mathbf a_z = \mathbf a_\rho \times \mathbf a_\phi$      
<img src="https://upload.wikimedia.org/wikipedia/commons/d/d2/Right_hand_rule_cross_product.svg" width="100" >  

ทิศทางของแกน $\rho \text{ และแกน }\phi$ จะเปลี่ยนทิศไปตามตำแหน่งของจุดเช่น แกนที่จุด $P$ และ แกนที่จุด $Q \text{ ในรูป } (d)$ มีทิศทางต่างกัน  

ในรูป $(c) \text{ จุด } P' \text{  เป็นจุดที่เคลื่อนจากจุด } P(\rho,\phi,z) \text { ออกมาเป็นระยะทางสั้นๆ เรียกว่า ความยาวดิฟเฟอเรนเชียล}$ (differential length) ในทิศทาง $\rho \text{ }\phi$ และ $z\text{ เรียกว่า }d\rho \text{ } \rho d\phi \text{ และ } dz \text{ เราต้องระวังว่า }d\phi$ คือการเปลี่ยนแปลงมุมมีหน่วยเป็นเรเดียน ไม่ใช่ระยะทางที่มีหน่วยเป็นเมตร เราจะต้องคูณรัศมีของการเปลี่ยนแปลงมุมเพื่อหาการเปลี่ยนแปลงของระยะทางโค้งๆ เช่นดาวเทียม2ดวงที่หมุนไป 
$d\phi$ เท่าๆกัน ดาวเทียมดวงที่อยู่ห่างจากโลกมากกว่าจะเคลื่อนที่เป็นระยะทางมากกว่า  
ดังนั้นจุด $P'$ จะมีชื่อในพิกัดแบบทรงกระบอกว่า $P'(\rho+d\rho,\phi+d\phi,z+dz)$ โดยมีความยาวดิฟเฟอเรนเชียลจากจุด $P \text{ ไปจุด }P'\text{ คือ }dl = \sqrt{d\rho^2+\left( \rho d\phi \right)^2+dz^2}$  

ในรูป $(c) \text{ มีระนาบอยู่ 6 ระนาบคือ 3 ระนาบที่ตัดกันที่จุด }P(\rho,\phi,z)\text{ และ 3 ระนาบที่ตัดกันที่จุด } P'(\rho+d\rho,\phi+d\phi,z+dz)$ รวมกันเป็นกล่องสี่เหลี่ยมเล็กๆที่มีปริมาตรเท่ากับ $dv = d\rho \times \rho d\phi \times dz \text{ หรือย้าย } \rho \text{ มาคู่กับ } d\rho \text{ เพื่อให้ง่ายต่อการอินทิเกรตจะได้ว่า }dv = \rho d\rho d\phi dz$  

พื้นผิวของกล่องสี่เหลี่ยมเล็กๆ เรียกว่า พื้นผิวดิฟเฟอเรนเชียล (differential area) ซึ่งมีอยู่สามพื้นผิวคือ $ds_\rho = \rho d\phi dz \text{ }ds_\phi = d\rho dz \text{ และ }ds_z = \rho d\rho d\phi$  

### [การแปลงชื่อของจุดระหว่างพิกัดฉาก $(x,y,z)$   กับพิกัดแบบทรงกระบอก $(\rho,\phi,z)$](ch1-06-00TH.md)  

### ตัวอย่างการประยุกต์ใช้พิกัดแบบทรงกระบอก  
1.6.1 [ตัวอย่างการหาความยาวของเส้นโค้ง](ch1-06-01TH.md)  
1.6.2 [ตัวอย่างการหาพื้นที่ครึ่งวงกลม](ch1-06-02TH.md)  
1.6.3 [ตัวอย่างการหาปริมาตรครึ่งทรงกลม](ch1-06-03TH.md)  

|[ก่อนหน้า](ch1-05TH.md)| [ต่อไป](ch1-06-00TH.md) |
| ---------- | ---------- |    

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/cylin_ab.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(a) แสดงพื้นผิวที่ตัวแปรในพิกัดแบบทรงกระบอกมีค่าคงที่ (b) แสดงทิศทางของแกน $\rho \text{ แกน }\phi \text{ และแกน } z \text{ ที่จุด } P$ (ดัดแปลงจาก [Hayt 2011](#Hayt))  

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/cylin_c.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(c) แสดงความยาวดิฟเฟอเรนเชียล (differential length) ปริมาตร ดิฟเฟอเรนเชียล (differential volume) พื้นผิวดิฟเฟอเรนเชียล (differential surface) (ดัดแปลงจาก [Hayt 2011](#Hayt))

<table align="center">
<tr><td align="center" width="9999">
<img src="./asset/cyline_cartesian.svg" align="center" width=50% alt="" />
</td></tr>
</table>  

(d) แสดงการแปลงชื่อของจุดระหว่างพิกัดฉากกับพิกัดแบบทรงกระบอก (differential surface) (ดัดแปลงจาก [Hayt 2011](#Hayt))

อ้างอิง  
- <a name="Hayt"></a> Hayt, W. H., & Buck, J. A. (2011). Engineering Electromagnetics (8th ed.). McGraw-Hill Professional.

|[ก่อนหน้า](ch1-05TH.md)| [ต่อไป](ch1-06-00TH.md) |
| ---------- | ---------- |    


[$\footnotesize \copyright 2020 \: \text {Denchai Worasawate}$](https://www.researchgate.net/profile/Denchai-Worasawate)  


