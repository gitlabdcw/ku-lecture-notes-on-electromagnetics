[$\footnotesize \copyright 2020 \: \text {Denchai Worasawate}$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |    

|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-05TH.md) |
| ---------- | ---------- |    

## 1.4 ส่วนประกอบเวกเตอร์และเวกเตอร์หนึ่งหน่วย (Vector Components and Unit Vectors)  
เวกเตอร์ในสามมิติมี 3 ส่วนประกอบที่ตั้งฉากกัน โดยแต่ละส่วนจะมีเวกเตอร์หนึ่งหน่วยกำกับทิศทางของส่วนประกอบนั้นๆ รูปด้านล่างแสดงเวกเตอร์ตำแหน่ง (position vector) ชี้ที่จุด $`(x,y,z)`$  
<img src="./asset/unitvector1.svg" width="300" >  
เราจะจินตนาการว่ามีเวกเตอร์หนึ่งหน่วย $`\mathbf a_x, \mathbf a_y, \mathbf a_z`$ ซึ่งเป็นเวกเตอร์หนึ่งหน่วยของพิกัดฉากกำกับอยู่ที่จุดนั้น เวกเตอร์ตำแหน่ง $`\mathbf r`$ มีส่วนของเวกเตอร์ในทิศทาง $`x`$ คือ $`\mathbf x = x \mathbf a_x`$ ส่วนของเวกเตอร์ในทิศทาง $`y`$ คือ $`\mathbf y = y \mathbf a_y`$ และ ส่วนของเวกเตอร์ในทิศทาง $`z`$ คือ $`\mathbf y = z \mathbf a_z`$  
รูปด้านล่างแสดงเวกเตอร์ตำแหน่งของจุด $`P`$ และจุด $`Q`$ เรียกว่า $`\mathbf r_P`$ และ $`\mathbf r_Q`$ ตามลำดับ`  
<img src="./asset/unitvector2.svg" width="300" >  
```math
\mathbf r_P = 1 \mathbf a_{xp}+ 2 \mathbf a_{yp}+ 3 \mathbf a_{zp} \newline
\mathbf r_Q = 2 \mathbf a_{xq}- 1 \mathbf a_{yq}+ 1 \mathbf a_{zq} \newline
```  
$`\mathbf a_{xp}, \mathbf a_{yp}, \mathbf a_{zp}`$ คือ เวกเตอร์หนึ่งหน่วยในพิกัดฉากที่จุด $`P`$ และ $`\mathbf a_{xq}, \mathbf a_{yq}, \mathbf a_{zq}`$ คือ เวกเตอร์หนึ่งหน่วยในพิกัดฉากที่จุด $`Q`$   
เนื่องจากพิกัดฉากเวกเตอร์ $`\mathbf a_x, \mathbf a_y, \mathbf a_z`$ ที่จุดใดๆ จะชี้ไปในทิศทางเดียวกัน เราจึงไม่ต้องใส่ชื่อจุดกำกับไว้ก็ได้ ซึ่งจะไม่เป็นจริงสำหรับเวกเตอร์หนึ่งหน่วยในพิกัดอื่นๆที่เราจะเรียนกัน สำหรับพิกัดอื่นๆเราจะต้องเปลี่ยนเวกเตอร์หนึ่งหน่วยในพิกัดนั้นให้เป็น $`\mathbf a_x, \mathbf a_y, \mathbf a_z`$ เราถึงจะสามารถนำเวกเตอร์มาบวกลบกันได้
เวกเตอร์ที่ชี้จากจุด $`P`$ ไปจุด $`Q`$ เรียกว่า $`\mathbf R_{PQ} = \mathbf r_Q - \mathbf r_P`$  

```math
\mathbf r_Q - \mathbf r_P= (2 \mathbf a_x- 1 \mathbf a_y+ 1 \mathbf a_z) -(1 \mathbf a_x+ 2 \mathbf a_y+ 3 \mathbf a_z) \newline
\mathbf R_{PQ}= (2-1) \mathbf a_x+(- 1-2) \mathbf a_y+ (1-3) \mathbf a_z) \newline
\mathbf R_{PQ}=  \mathbf a_x-3 \mathbf a_y-2 \mathbf a_z \newline
```  
ถ้าเรามีเวกเตอร์ $`\mathbf A`$ ที่เขียนอยู่ในรูปของส่วนประกอบได้เป็น $`\mathbf A = A_x \mathbf a_x+ A_y \mathbf a_y+ A_z \mathbf a_z`$ เราเรียก $`A_x,A_y,A_z`$ ว่าส่วนประกอบของเวกเตอร์ $`\mathbf A `$ ในทิศ $`x,y,z`$ ตามลำดับ ขนาดของ $`\mathbf A`$ แทนด้วย $`|\mathbf A|`$ และทิศทางของ $`\mathbf A`$ บอกเวกเตอร์หนึ่งหน่วยในทิศทางเดียวกับ $`\mathbf A`$ โดยแทนด้วย $`\mathbf a_A`$ เราสามารถเขียน $`\mathbf A = |\mathbf A| \mathbf a_A`$ โดย $`|\mathbf A|`$ หาได้จาก  
```math
|\mathbf A| = \sqrt{A_x^2+A_y^2+A_z^2}
```  
และ $`\mathbf a_A`$  ทิศทางของ $`\mathbf A`$ หาได้จาก
```math
\mathbf a_A = \frac {\mathbf A}{|\mathbf A|}
``` 


|[ก่อนหน้า](ch1-03TH.md)| [ต่อไป](ch1-05TH.md) |
| ---------- | ---------- |    


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


