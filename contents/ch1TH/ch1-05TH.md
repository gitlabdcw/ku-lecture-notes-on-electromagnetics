[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |  

|[ก่อนหน้า](ch1-04TH.md)| [ต่อไป](ch1-05-1TH.md) |
| ---------- | ---------- |    

## 1.5 การคูณเวกเตอร์ (Vector Multiplication)  
การคูณเวกเตอร์กับเวกเตอร์ด้วยกันมีอยู่สองแบบคือการคูณแบบดอท (dot product) ซึ่งให้ผลลัพธ์เป็นปริมาณสเกลาร์หรือเรียกอีกชื่อว่าการคูณเชิงสเกลาร์ (scalar product) และการคูณแบบครอส (cross product) ซึ่งให้ผลลัพธ์เป็นปริมาณเวกเตอร์หรือเรียกอีกชื่อว่าการคูณเชิงเวกเตอร์ (vector product)  
### 1.5.1 [การคูณแบบดอท (dot product)](ch1-05-1TH.md)
### 1.5.2 [การคูณแบบครอส (cross product)](ch1-05-2TH.md)
  


|[ก่อนหน้า](ch1-04TH.md)| [ต่อไป](ch1-05-1TH.md) |
| ---------- | ---------- |    


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  

