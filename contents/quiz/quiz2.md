เราสามารถใช้ Gauss' law หาสนามไฟฟ้าที่เกิดจากประจุในข้อใด  
Among the given charges, which one is suitable for applying Gauss' law to calculate the electric field?  

1) $\rho_l = \sin \phi$ where $\rho = 1$ on $xy$-plane  
2) $\rho_v = x$ where $`\lvert x \rvert < 1`$  
3) $\rho_s = \sin \phi$ where $`\rho < 1`$ on $xy$-plane  
4) $\rho_v = y$ where $`\lvert x \rvert < 1`$