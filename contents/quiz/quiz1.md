ข้อใดไม่ใช่จุดเดียวกับจุด $(x,y,z) = (3,4,12)$  
Which one is not the same point as $(x,y,z) = (3,4,12)$?  
1) $(\rho,\phi,z) = (5,\tan^{-1} \left( \frac{4}{3}\right),12)$  
2) $(\rho,\phi,z) = (5,\sin^{-1} \left( \frac{4}{5}\right),12)$  
3) $(r,\theta, \phi) = (13,\cos^{-1} \left( \frac{12}{13}\right),\cos^{-1} \left( \frac{4}{3}\right))$  
4) $(r,\theta, \phi) = (13,\tan^{-1} \left( \frac{5}{12}\right),\tan^{-1} \left( \frac{4}{3}\right))$  

เวกเตอร์ในข้อใดมีทิศทางต่างจากกับเวกเตอร์ $\mathbf A = 2\mathbf a_{\rho}$ กระทำที่จุด $(x,y,z) = (2,2,2 \sqrt 2)$  
Which vector does not have the same direction as vector $\mathbf A = 2\mathbf a_{\rho}$ acting at $(x,y,z) = (2,2,2 \sqrt 2)$?  
1) $\mathbf B = 2 \mathbf a_x+ 2\mathbf a_y$ กระทำที่จุด $(x,y,z) = (2,2,2 \sqrt 2)$  
2) $\mathbf C = 2 \mathbf a_x+ 2\mathbf a_y$ กระทำที่จุด $(x,y,z) = (0,0,2)$  
3) $\mathbf D = 2 \mathbf a_{\rho}+ 2\mathbf a_{\phi}$ กระทำที่จุด $(x,y,z) = (2,0,0)$  
4) $\mathbf E = 2 \mathbf a_r+ 2\mathbf a_{\phi}$ กระทำที่จุด $(x,y,z) = (0,0,2)$  
 

$\displaystyle\int_0^\pi {\frac{d \phi}{((0.5-\cos \phi)^2 + \sin^2 \phi)^\frac{3}{2}}}$ มีค่าประมาณเท่าใด  
What is the approximated value of $\displaystyle\int_0^\pi {\frac{d \phi}{((0.5-\cos \phi)^2 + \sin^2 \phi)^\frac{3}{2}}}$?  
1) 3.9  
2) 4.9  
3) 5.9  
4) 6.9
 

ออกแรง $\mathbf F = x \mathbf a_x + y \mathbf a_y$ N ดันก้อนหินจากจุด $A(x,y,z) = A(3,0,0)$ ไปจุด $B(x,y,z) = B(0,3,0)$ โดยงานที่ทำมีค่าเท่ากับ $\displaystyle\int_{A}^{B}\mathbf F \cdot d\mathbf l$ ข้อใดไม่ใช่เป็นสมการหางานที่เกิดขึ้น   
1.  $\displaystyle \int_{(x,y)=(3,0)}^{(x,y)=(0,3)} {\left(xdx+ydy\right)}$  
  
2.  $\displaystyle \int_{x=3}^{x=0} (2x-3)dx$ เมื่อเส้นทางในการดันก้อนหินเป็นสมการ $y=-x+3$  
  
3. $\displaystyle \int_{x=3}^{x=0} xdx +\int_{y=0}^{y=3} ydy$ เมื่อดันก้อนหินตามแนวแกน $x$ และ แกน $y$    
  
4. $\displaystyle \int_{\phi=0}^{\phi=\frac{\pi}{2}} 9d \phi$ เมื่อดันตามเส้นวงกลมรัศมีเท่ากับ 3    
